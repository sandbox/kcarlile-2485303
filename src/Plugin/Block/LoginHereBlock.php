<?php
namespace Drupal\login_here_block\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Login Here Block
 *
 * @Block(
 *   id = "login_here_block",
 *   admin_label = @Translation("Login Here Block"),
 * )
 */
class LoginHereBlock extends BlockBase implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['contents'])) {
      $contents = $config['contents'];
    } // end if test
    else {
      $contents = $this->t('Login here');
    } // end else test

    $current_url = \Drupal::request()->getRequestUri();

    return array(
      '#theme' => 'login_here_block',
      '#current_url' => $current_url,
      '#contents' => $contents,
    );
  } // end function build

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['login_here_block_contents'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('Link contents'),
      '#description' => $this->t('The contents of the link, such as "Login here."'),
      '#default_value' => isset($config['contents']) ? $config['contents'] : 'Login here',
    );

    return $form;
  } // end function blockForm

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('contents', $form_state->getValue('login_here_block_settings'));
  } // end function blockSubmit

  public function defaultConfiguration() {
    $default_config = \Drupal::config('login_here_block.settings');
    return array(
      'login_here_block_settings' => $default_config->get('login_here_block.contents')
    );
  } // end function defaultConfiguration
} // end class LoginHereBlock